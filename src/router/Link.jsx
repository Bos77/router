import React from "react";


export const Link = ({to , state = {}, children, ...rest}) => {
    const handleClick = (e) => {
        e.preventDefault();
        window.history.pushState(state, "", to);
    };

    return <a href={to} onClick={handleClick} {...rest}>{children}</a>
}