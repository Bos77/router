import React, {useContext} from "react";

import {RouterContext} from "./AppRouter"


export const Route = ({path, children}) => {
    const routerContext = useContext(RouterContext);


    if(path === routerContext.location.pathname) {
        return children
    }

    return null;
}