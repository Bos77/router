import {system} from "../actions"

const initState = {
    lngCode: "uz",
    theme: "dark",
};

export const systemReducer = (state = initState, action) => {
    const { type, payload } = action;

    switch (type) {
        case system.LNG:
            return { ...state, lngCode: payload };

        case system.THEME:
            return { ...state, theme: payload };

        default:
            return { ...state };
    }
};
