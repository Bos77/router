import './App.css';
import {AppRouter} from "./router/AppRouter";
import {Link} from "./router/Link";
import {useDispatch} from "react-redux";
import {useEffect} from "react";
import {system} from "./store/actions";
import {Route} from "./router/Route";


function App() {
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(system.changeTheme("light"))
    }, [])



  return (
    <AppRouter>
        <Route path="/about-us" ><h1>ABOUT US</h1></Route>
        <Route path="/career" ><h1>Career</h1></Route>
        <Route path="/contacts" ><h1>Contacts</h1></Route>

      <Link to="/about-us" >ABout us</Link>
      <Link to="/career" >Career</Link>
      <Link to="/contacts" >Contacts</Link>
    </AppRouter>
  );
}

export default App;
