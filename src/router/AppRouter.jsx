import React, {createContext, useEffect, useState} from "react";

export const RouterContext = createContext(null)


export const AppRouter = ({children}) => {
    const [location, setLocation] = useState(window.location)


    const onLocationChange = () => {
        setLocation({...window.location})
    }

    useEffect(() => {
        window.addEventListener("locationchange",  onLocationChange)
        return () => window.removeEventListener("locationchange", onLocationChange);
    }, [])



    return <RouterContext.Provider value={{location}}>
        {children}
    </RouterContext.Provider>
}